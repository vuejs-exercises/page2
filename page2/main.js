const app = new Vue({
    el: '#app',
    data: {
        titulo: 'Hola mundo con Vue',
        frutas: [
            {nombre: 'Banano', stock: 15},
            {nombre: 'Manzana', stock: 8},
            {nombre: 'Pera', stock: 0},
            {nombre: 'Mandarina', stock: 1}
        ],
        nuevaFruta: '',
        total: 0
    },
    methods: {
        agregarFruta () {
            this.frutas.push({
                nombre: this.nuevaFruta, stock: 0
            });
            this.nuevaFruta = '';
        }
    },
    // Se ejecuta cada ocasión que se detecta un cambio en alguna de sus propiedades
    computed: {
        sumarFrutas () {
            this.total = 0;
            for(fruta of this.frutas){
                this.total = this.total + fruta.stock;
            }
            // Para actualizar la vista es necesario agregar un return con el dato que deso mostrar
            return this.total;
        }
    }
})

